require 'byebug'
class Book
  @@lowercase_words = [
    "and",
    "in",
    "the",
    "of",
    "a",
    "an"
  ]
  attr_reader :title

  def title=(name)
    @title = name.split(" ").map do |word|
      @@lowercase_words.include?(word) ? word : word.capitalize
    end
    @title[0] = @title[0].capitalize
    @title = @title.join(" ")
  end
end
