require 'byebug'

class Temperature
  class << self
    def from_fahrenheit(temp)
      new(:f => temp)
    end

    def from_celsius(temp)
      new(:c => temp)
    end
  end

  attr_accessor :temp

  def initialize(options = {})
    @temp = options[:f] unless options[:f].nil?
    @temp = (options[:c] * (9.to_f / 5) + 32) unless options[:c].nil?
  end

  def in_fahrenheit
    # @temp
    self.class.ctof(@temp)
  end

  def in_celsius
    # ((@temp - 32) * (5.to_f/9))
    self.class.ftoc(@temp)
  end

  def self.ftoc(temp)
    ((temp - 32) * (5.to_f/9))
  end

  def self.ctof(temp)
    temp
  end
end

class Celsius < Temperature
  def initialize(temp)
    @temp = temp * (9.to_f / 5) + 32
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @temp = temp
  end
end
