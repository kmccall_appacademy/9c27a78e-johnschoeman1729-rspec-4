class Dictionary
  attr_reader :entries, :keywords

  def initialize
    @entries = {}
    @keywords = []
  end

  def add(entry)
    if entry.is_a? Hash
      @entries.merge!(entry)
      @keywords.push(entry.keys[0]).sort!
    elsif entry.is_a? String
      @entries[entry] = nil
      @keywords.push(entry).sort!
    end
  end

  def include?(entry)
    @keywords.include?(entry)
  end

  def find(entry)
    @entries.select { |k, _| k =~ /#{entry}/ }
  end

  def printable
    str = ""
    @keywords.each do |k|
      str += "[#{k}] \"#{@entries[k]}\"\n"
    end
    str.chomp
  end
end
